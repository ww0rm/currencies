<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <div class="alert alert-danger alert-dismissible fade in" role="alert" style="display:none;">
                    <strong>Error!</strong>
                    <span id="error-message"></span>
                </div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Currency</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody id="favorites-list"></tbody>
                </table>
                <select id="currencies-list" class="col-md-8"></select>
                <button id="add-currency" class="col-md-3 pull-right">Add</button>
            </div>
        </div>

    </div>
</div>
