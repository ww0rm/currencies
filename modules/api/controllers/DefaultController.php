<?php

namespace app\modules\api\controllers;

use app\models\Favorites;
use app\models\Currency;
use yii\rest\Controller;
use yii\web\HttpException;

class DefaultController extends Controller
{
    /**
     * return all available currencies
     * @return array|Currency[]
     */
    public function actionCurrencies()
    {
        return Currency::find()->all();
    }

    /**
     * return favorite currencies for user
     * @return array|Favorites[]
     */
    public function actionList()
    {
        return Favorites::find()->where(['guest_id' => \Yii::$app->user->getId()])->with('currency')->all();
    }

    /**
     * add currency to favorites
     * @param int $currency_id
     * @return Favorites
     * @throws HttpException (400, 403)
     */
    public function actionAdd($currency_id = 0)
    {
        $c = Favorites::find()->where(['guest_id' => \Yii::$app->user->getId()])->count();
        if ($c >= 10) {
            throw new HttpException(403, 'You can`t add more that 10 currencies');
        }

        $c = Favorites::find()->where(['guest_id' => \Yii::$app->user->getId(), 'currency_id' => $currency_id])->count();
        if ($c > 0) {
            throw new HttpException(400, 'Currency already in your favorites');
        }

        $favorite = new Favorites();
        $favorite->currency_id = $currency_id;
        $favorite->guest_id = \Yii::$app->user->getId();
        $favorite->save();

        return $favorite;
    }

    /**
     * remove currency from favorites
     * @param int $currency_id
     * @return array|Favorites[]
     */
    public function actionRemove($currency_id = 0)
    {
        $favorite = Favorites::find()->where(['guest_id' => \Yii::$app->user->getId(), 'currency_id' => $currency_id])->one();
        if (!is_null($favorite)) {
            $favorite->delete();
        }

        return $this->actionList();
    }
}
