/**
 * Created by ww0rm on 2/20/17.
 */

function CurrencyWidget() {
    this.currencies = [];
    this.favorites = [];
    this.favoritesList = $('#favorites-list');
    this.currenciesList = $('#currencies-list');
    this.addCurrencyBtn = $('#add-currency');
    this.error = $('#error-message');
}

/*
    init widget
 */
CurrencyWidget.prototype.init = function () {
    this.loadCurrencies();
    this.loadFavorites();

    // set add new currency button callback
    this.addCurrencyBtn.click(function () {
        this.addItem(this.currenciesList.val());
    }.bind(this));
};

/*
    load available currencies
 */
CurrencyWidget.prototype.loadCurrencies = function () {
    $.ajax('/api/currencies')
        .done(function (data) {
            this.currencies = data; // set currencies data to object
            this.renderCurrencies(); // render
        }.bind(this))
        .fail(function (xhr) {
            this.showError(xhr);
        }.bind(this));
};

/*
    render currencies into select
 */
CurrencyWidget.prototype.renderCurrencies = function () {
    // clear currency select
    this.currenciesList.empty();

    // add currency option to select
    this.currencies.forEach(function (item) {
        this.currenciesList.append($("<option></option>").attr("value", item.id).html(item.title));
    }, this);
};

/*
    load user favorites currencies
 */
CurrencyWidget.prototype.loadFavorites = function () {
    $.ajax('/api/list')
        .done(function (data) {
            this.favorites = data; // set favorites data to object
            this.renderFavorites(); // render
        }.bind(this))
        .fail(function (xhr) {
            this.showError(xhr);
        }.bind(this));
};

/*
    render favorites
 */
CurrencyWidget.prototype.renderFavorites = function () {
    // clear favorites table
    this.favoritesList.empty();

    // add favorite items to table
    this.favorites.forEach(function (item) {
        this.renderFavoriteItem(item); // render item
    }, this);
};

/*
    render favorite row
 */
CurrencyWidget.prototype.renderFavoriteItem = function (item) {
    var row = $("<tr></tr>");
    row.append($("<td></td>").html(item.currency.title));
    row.append($("<td></td>").append(
        $("<button></button>").html("delete").click(function () {
            this.removeItem(item);
        }.bind(this))) // set callback to delete button
    );

    this.favoritesList.append(row);
};

/*
    add currency to favorites
 */
CurrencyWidget.prototype.addItem = function (id) {
    $.ajax('/api/add', {data: {currency_id: id}})
        .done(function (data) {
            this.loadFavorites(); // load favorites after add new currenct
        }.bind(this))
        .fail(function (xhr) {
            this.showError(xhr);
        }.bind(this));
};

/*
    remove currency from favorites
 */
CurrencyWidget.prototype.removeItem = function (item) {
    $.ajax('/api/remove', {data: {currency_id: item.currency_id}})
        .done(function (data) {
            this.favorites = data; // set favorites data to object
            this.renderFavorites(); // render
        }.bind(this))
        .fail(function (xhr) {
            this.showError(xhr);
        }.bind(this));
};

/*
    show error message
 */
CurrencyWidget.prototype.showError = function (xhr) {
    var err = JSON.parse(xhr.responseText); // parse response text to JSON object

    this.error.html(err.message).parent().show(); // add error message and show alert

    // hide alert after 3s
    setTimeout(function () {
        this.error.parent().hide();
    }.bind(this), 3000);
}

$(document).ready(function () {
    var currencyWidget = new CurrencyWidget();
    currencyWidget.init();
});