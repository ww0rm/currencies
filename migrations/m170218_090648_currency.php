<?php

use yii\db\Migration;

class m170218_090648_currency extends Migration
{
    public function up()
    {
        $this->createTable("currency", [
            'id' => 'pk',
            'title' => 'string',
            'image' => 'string',
            'payment_system_id' => 'int'
        ]);

        $this->addForeignKey('c_payment_system', 'currency', 'payment_system_id', 'payment_system', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('c_payment_system', 'currency');
        $this->dropTable('currency');
    }
}
