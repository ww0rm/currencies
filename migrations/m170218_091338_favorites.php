<?php

use yii\db\Migration;

class m170218_091338_favorites extends Migration
{
    public function up()
    {
        $this->createTable('favorites', [
            'id' => 'pk',
            'currency_id' => 'int',
            'guest_id' => 'string'
        ]);

        $this->addForeignKey('f_currency', 'favorites', 'currency_id', 'currency', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('f_currency', 'favorites');
        $this->dropTable('favorites');
    }
}
