<?php

use yii\db\Migration;

class m170218_090642_payment_system extends Migration
{
    public function up()
    {
        $this->createTable('payment_system', [
            'id' => 'pk',
            'title' => 'string',
            'image' => 'string',
        ]);
    }

    public function down()
    {
        $this->dropTable('payment_system');
    }
}
