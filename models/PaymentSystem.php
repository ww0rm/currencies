<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_system".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 *
 * @property Currency[] $currencies
 */
class PaymentSystem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_system';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencies()
    {
        return $this->hasMany(Currency::className(), ['payment_system_id' => 'id']);
    }
}
