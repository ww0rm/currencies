<?php
/**
 * Created by PhpStorm.
 * User: ww0rm
 * Date: 2/20/17
 * Time: 09:18
 */

namespace app\models;

use Yii;

class WebUser extends \yii\web\User
{
    /**
     * return user_id or generate and save to cookie unique user guid
     * @return int|string
     */
    public function getId()
    {
        if ($this->isGuest && !Yii::$app->request->cookies->has("guest_id")) {
            $uid = md5(uniqid(rand(), TRUE) . time());
            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'guest_id',
                'value' => $uid,
                'expire' => time() + (60 * 60 * 24 * 365),
            ]));
        }

        return $this->isGuest ? Yii::$app->request->cookies->get("guest_id")->value : parent::getId();
    }
}