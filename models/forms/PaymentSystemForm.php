<?php
/**
 * Created by PhpStorm.
 * User: ww0rm
 * Date: 2/20/17
 * Time: 01:32
 */

namespace app\models\forms;


use app\models\PaymentSystem;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class PaymentSystemForm
 * @package app\models\forms
 */
class PaymentSystemForm extends Model
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $image;
    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @var PaymentSystem
     */
    private $model;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = $this->model->rules();
        $rules[] = [['imageFile'], 'file', 'extensions' => 'png, jpg'];

        return $rules;
    }

    /**
     * upload image file and return file name
     * @return string
     */
    public function upload()
    {
        if ($this->validate()) {
            $fileName = 'uploads/' . time() . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs($fileName);

            return $fileName;
        } else {
            return null;
        }
    }

    /**
     * save model
     */
    public function save()
    {
        $this->imageFile = UploadedFile::getInstance($this, 'imageFile');

        $this->model->setAttributes($this->getAttributes());

        if (!empty($this->imageFile)) {
            $this->model->image = $this->upload();
        }

        $this->model->save();
    }

    /**
     * @return PaymentSystem
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param PaymentSystem $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }
}