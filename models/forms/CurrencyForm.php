<?php
/**
 * Created by PhpStorm.
 * User: ww0rm
 * Date: 2/20/17
 * Time: 00:53
 */

namespace app\models\forms;


use app\models\Currency;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class CurrencyForm
 * @package app\models\forms
 */
class CurrencyForm extends Model
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $image;
    /**
     * @var int
     */
    public $payment_system_id;
    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @var Currency
     */
    private $model;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = $this->model->rules();
        $rules[] = [['imageFile'], 'file', 'extensions' => 'png, jpg'];

        return $rules;
    }

    /**
     * upload image file and return file name
     * @return string
     */
    public function upload()
    {
        if ($this->validate()) {
            $fileName = 'uploads/' . time() . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs($fileName);

            return $fileName;
        } else {
            return null;
        }
    }

    /**
     *  save model
     */
    public function save()
    {
        $this->imageFile = UploadedFile::getInstance($this, 'imageFile');

        $this->model->setAttributes($this->getAttributes());

        if (!empty($this->imageFile)) {
            $this->model->image = $this->upload();
        }

        $this->model->save();
    }

    /**
     * @return Currency
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param Currency $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }
}